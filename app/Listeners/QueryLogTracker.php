<?php

// copy based : https://qiita.com/n-oota/items/d2a05e967fa01743f103

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use DateTime;
use DB;
use Log;

/**
 * クエリログ出力
 *
 * @package app.Listeners
 */
class QueryLogTracker
{
    /**
     * Handle the event.
     *
     * @param QueryExecuted $event
     * @internal param $query
     * @internal param $bindings
     * @internal param $time
     */
    public function handle(QueryExecuted $event)
    {
        // クエリ情報を見やすく整形
        $time = $event->time;
        $bindings = $event->bindings;
        foreach ($bindings as $i => $binding) {
            if ($binding instanceof DateTime) {
                $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
            } else if (is_string($binding)) {
                $bindings[$i] = DB::getPdo()->quote($binding);
            } else if (null === $binding) {
                $bindings[$i] = 'null';
            }
        }
        $query = str_replace(array('%', '?', "\r", "\n", "\t"), array('%%', '%s', ' ', ' ', ' '), $event->sql);
        $query = preg_replace('/\s+/uD', ' ', $query);
        if (preg_match('/^select/i', $query) == 1) { // SELECT文のみ値を表示
            $query = vsprintf($query, $bindings) . ';';
        }

        // Trace
        $trace = debug_backtrace();
        for($i = 1; $i < count($trace); ++$i) {
            if (isset($trace[$i]['file']) && strpos($trace[$i]['file'], 'vendor/laravel') === FALSE) {
                break;
            }
        }
        $filepos = isset($trace[$i]) ? $trace[$i]['file'] . ':' . $trace[$i]['line'] : 'Unknown';

        // ログへ出力
        Log::debug($query, compact('time', 'filepos'));
    }
}
